package Abstraction;

public class LoanAccount implements Accounts{
    double accountBalance;

    public LoanAccount(double accountBalance) {
        this.accountBalance = accountBalance;
        System.out.println("Loan Account Created Succesfully");
    }

    @Override
    public void deposit(double amt) {
        accountBalance-=amt;
        System.out.println(amt+" Rs EMI Installment Succesfully Deposited");
    }

    @Override
    public void withdraw(double amt) {
    if(amt<=accountBalance){
        accountBalance+=amt;
        System.out.println(amt+" Rs Loan Amount Withdrawn Successfully");
    }else{
        System.out.println("Insufficient Balance");
    }
    }

    @Override
    public void checkBalance() {
        System.out.println("Loan Amount: "+accountBalance);
    }
}
