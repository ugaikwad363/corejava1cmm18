package Abstraction;

public class AccountFactory {
    void transactions(){
        SavingsAccount s1=new SavingsAccount(0);
        s1.deposit(5000);
        s1.withdraw(2000);
        s1.checkBalance();
        System.out.println("=================================");
        LoanAccount l1=new LoanAccount(100000);
        l1.deposit(25000);
        l1.withdraw(10000);
        l1.checkBalance();

    }
}
