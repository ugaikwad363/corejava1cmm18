package Abstraction;

public interface Accounts {
    void deposit(double amt);
    void withdraw(double amt);
    void checkBalance();
}
