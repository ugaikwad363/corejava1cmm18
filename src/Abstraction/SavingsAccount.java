package Abstraction;

public class SavingsAccount implements Accounts{
    double accountBalance;

    public SavingsAccount(double accountBalance) {
        this.accountBalance = accountBalance;
        System.out.println("Savings Account Created Succesfully");
    }

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+" Rs Credited to Your Account");
    }

    @Override
    public void withdraw(double amt) {
    if(amt<=accountBalance){
        accountBalance-=amt;
        System.out.println(amt+" Rs Withdrawn From Your Account");
    }else {
        System.out.println("INSUFFICIENT BALANCE");
    }
    }

    @Override
    public void checkBalance() {
        System.out.println("Saving Amount: "+accountBalance);
    }
}
